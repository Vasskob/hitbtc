package com.oliver.hitbtctrader.screens.symbols.list.view;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.PresenterType;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.oliver.hitbtctrader.R;
import com.oliver.hitbtctrader.domain.SymbolsRepository;
import com.oliver.hitbtctrader.domain.model.Symbol;
import com.oliver.hitbtctrader.screens.common.BaseFragment;
import com.oliver.hitbtctrader.screens.symbols.list.presenter.SymbolsListPresenter;
import com.oliver.hitbtctrader.screens.symbols.list.view.adapter.SymbolsListAdapter;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SymbolsListFragment extends BaseFragment implements SymbolsListView {

    private static final String TAG = SymbolsListFragment.class.getSimpleName();
    private SymbolsListAdapter mAdapter;


    @BindView(R.id.rv_symbols_list)
    RecyclerView mSymbolsListRecyclerView;

    @Inject
    SymbolsRepository mSymbolsRepository;

    @InjectPresenter(type = PresenterType.LOCAL)
    SymbolsListPresenter mPresenter;


    @ProvidePresenter(type = PresenterType.LOCAL)
    SymbolsListPresenter provideTmpPresenter() {
        return new SymbolsListPresenter(mSymbolsRepository);
    }

    private OnSymbolsListInteractionListener mCallback;


    public static SymbolsListFragment newInstance() {
        return new SymbolsListFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_symbols_list, container, false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mCallback = ((OnSymbolsListInteractionListener) context);
        } catch (ClassCastException e) {
            throw new RuntimeException(context + " must implement OnSymbolsListInteractionListener");
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        initList();
    }

    private void initList() {
        mAdapter = new SymbolsListAdapter((itemView, item) -> {
            Log.d(TAG, "onItemClick() called with: itemView = [" + itemView + "], item = [" + item + "]");
            mCallback.onSymbolSelected(item);
        });
        mSymbolsListRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        mSymbolsListRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        mSymbolsListRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mSymbolsListRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onFirstViewAttach() {
        mPresenter.loadSymbolsList();
    }

    @Override
    public void showSymbolsList(List<Symbol> symbolList) {
        Log.d(TAG, "showSymbolsList: " + symbolList);
        mAdapter.setData(symbolList);
    }

    @Override
    public void showLoadingError() {
        Toast.makeText(getActivity(), R.string.err_symbols_list_loading, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoadingDialog() {
        showLoader();
    }

    @Override
    public void dismissLoadingDialog() {
        dismissLoader();
    }


    public interface OnSymbolsListInteractionListener {
        void onSymbolSelected(Symbol symbol);
    }
}
