package com.oliver.hitbtctrader.data.local;


import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.oliver.hitbtctrader.data.local.model.content.SymbolEntity;
import com.oliver.hitbtctrader.data.local.model.dao.SymbolsDAO;

@Database(entities = {SymbolEntity.class}, version = 1)
public abstract class HitBTCDatabase extends RoomDatabase {
    public abstract SymbolsDAO symbolsDao();
//    public abstract TicketsDAO ticketsDao();
}
