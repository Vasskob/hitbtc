package com.oliver.hitbtctrader.domain.model;


public class Ticker {

    private String last;
    private String bid;
    private String ask;
    private String high;
    private String low;
    private String volume;
    private String open;
    private String volumeQuote;
    private long timestamp;

    public String getLast() {
        return last;
    }

    public void setLast(String last) {
        this.last = last;
    }

    public String getBid() {
        return bid;
    }

    public void setBid(String bid) {
        this.bid = bid;
    }

    public String getAsk() {
        return ask;
    }

    public void setAsk(String ask) {
        this.ask = ask;
    }

    public String getHigh() {
        return high;
    }

    public void setHigh(String high) {
        this.high = high;
    }

    public String getLow() {
        return low;
    }

    public void setLow(String low) {
        this.low = low;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public String getOpen() {
        return open;
    }

    public void setOpen(String open) {
        this.open = open;
    }

    public String getVolumeQuote() {
        return volumeQuote;
    }

    public void setVolumeQuote(String volumeQuote) {
        this.volumeQuote = volumeQuote;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Ticker)) return false;

        Ticker ticker = (Ticker) o;

        if (timestamp != ticker.timestamp) return false;
        if (last != null ? !last.equals(ticker.last) : ticker.last != null) return false;
        if (bid != null ? !bid.equals(ticker.bid) : ticker.bid != null) return false;
        if (ask != null ? !ask.equals(ticker.ask) : ticker.ask != null) return false;
        if (high != null ? !high.equals(ticker.high) : ticker.high != null) return false;
        if (low != null ? !low.equals(ticker.low) : ticker.low != null) return false;
        if (volume != null ? !volume.equals(ticker.volume) : ticker.volume != null) return false;
        if (open != null ? !open.equals(ticker.open) : ticker.open != null) return false;
        return volumeQuote != null ? volumeQuote.equals(ticker.volumeQuote) : ticker.volumeQuote == null;
    }

    @Override
    public int hashCode() {
        int result = last != null ? last.hashCode() : 0;
        result = 31 * result + (bid != null ? bid.hashCode() : 0);
        result = 31 * result + (ask != null ? ask.hashCode() : 0);
        result = 31 * result + (high != null ? high.hashCode() : 0);
        result = 31 * result + (low != null ? low.hashCode() : 0);
        result = 31 * result + (volume != null ? volume.hashCode() : 0);
        result = 31 * result + (open != null ? open.hashCode() : 0);
        result = 31 * result + (volumeQuote != null ? volumeQuote.hashCode() : 0);
        result = 31 * result + (int) (timestamp ^ (timestamp >>> 32));
        return result;
    }
}
