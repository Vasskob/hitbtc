package com.oliver.hitbtctrader.screens.tmp.di;


import com.oliver.hitbtctrader.screens.tmp.view.TmpFragment;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;


@Subcomponent
public interface TmpSubComponent extends AndroidInjector<TmpFragment> {
    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<TmpFragment> {
    }
}

