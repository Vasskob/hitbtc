package com.oliver.hitbtctrader.screens.symbols.ticker.di;


import com.oliver.hitbtctrader.screens.symbols.ticker.view.SymbolTickerFragment;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;

@Subcomponent
public interface SymbolTickerSubComponent extends AndroidInjector<SymbolTickerFragment> {
    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<SymbolTickerFragment> {}
}
