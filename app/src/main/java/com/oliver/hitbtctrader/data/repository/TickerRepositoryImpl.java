package com.oliver.hitbtctrader.data.repository;


import com.oliver.hitbtctrader.data.api.PublicApi;
import com.oliver.hitbtctrader.data.api.model.mapper.TickerApiModelToTickerMapper;
import com.oliver.hitbtctrader.domain.TickerRepository;
import com.oliver.hitbtctrader.domain.model.Ticker;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class TickerRepositoryImpl implements TickerRepository {
    private final PublicApi mApi;

    @Inject
    public TickerRepositoryImpl(PublicApi api) {
        mApi = api;
    }

    @Override
    public Observable<Ticker> getTicker(String symbol) {
        return mApi.getSymbolTicker(symbol)
                .subscribeOn(Schedulers.newThread())
                .map(new TickerApiModelToTickerMapper())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
