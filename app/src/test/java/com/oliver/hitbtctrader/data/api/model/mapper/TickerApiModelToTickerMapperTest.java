package com.oliver.hitbtctrader.data.api.model.mapper;

import com.oliver.hitbtctrader.data.api.model.content.TickerApiModel;
import com.oliver.hitbtctrader.domain.model.Ticker;
import com.oliver.hitbtctrader.domain.test.TickerTestData;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static junit.framework.Assert.assertTrue;

@RunWith(JUnit4.class)
public class TickerApiModelToTickerMapperTest {

    private TickerApiModelToTickerMapper mMapper;
    private TickerTestData mData;

    @Before
    public void setup() {
        mData = new TickerTestData();
        mMapper = new TickerApiModelToTickerMapper();
    }

    @Test
    public void testMapper() throws Exception {
        int index = mData.getRandomIndex();

        TickerApiModel sourceModel = mData.getTickerApiModel(index);
        Ticker expectedModel = mData.getTickerModel(index);

        Ticker resultModel = mMapper.apply(sourceModel);

        assertTrue("Mapper converting error", expectedModel.equals(resultModel));

    }

}
