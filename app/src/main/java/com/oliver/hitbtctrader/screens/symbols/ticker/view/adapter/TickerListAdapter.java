package com.oliver.hitbtctrader.screens.symbols.ticker.view.adapter;


import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oliver.hitbtctrader.R;
import com.oliver.hitbtctrader.domain.model.Ticker;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TickerListAdapter extends RecyclerView.Adapter<TickerListAdapter.TickerItemViewHolder> {
    private static final SimpleDateFormat mDateFormat = new SimpleDateFormat("dd/MM/YYYY HH:mm:ss:SSS", Locale.US);
    private List<Ticker> mData = new ArrayList<>();

    public TickerListAdapter() {
    }

    public void addOnTop(Ticker ticker) {
        mData.add(0, ticker);
        notifyItemInserted(0);
    }

    public void setData(@NonNull Ticker item) {
        mData.add(item);
        notifyItemInserted(mData.size());
    }

    public void setData(@NonNull List<Ticker> data) {
        mData.addAll(data);
        notifyDataSetChanged();
    }

    public void clearData() {
        mData.clear();
        notifyDataSetChanged();
    }


    @Override
    public TickerItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TickerItemViewHolder( LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_ticker, parent, false));
    }

    @Override
    public void onBindViewHolder(TickerItemViewHolder holder, int position) {
        Ticker ticker = mData.get(position);
        holder.bind(ticker);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    class TickerItemViewHolder extends RecyclerView.ViewHolder {
        private final Resources mRes;

        @BindView(R.id.tv_last)
        TextView lastTextView;
        @BindView(R.id.tv_bid)
        TextView bidTextView;
        @BindView(R.id.tv_ask)
        TextView askTextView;
        @BindView(R.id.tv_high)
        TextView highTextView;
        @BindView(R.id.tv_low)
        TextView lowTextView;
        @BindView(R.id.tv_timestamp)
        TextView timestampTextView;


        public TickerItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            mRes = itemView.getResources();
        }

        public void bind(Ticker ticker) {
            lastTextView.setText(mRes.getString(R.string.ticker_last_prefix, ticker.getLast()));
            bidTextView.setText(mRes.getString(R.string.ticker_bid_prefix, ticker.getBid()));
            askTextView.setText(mRes.getString(R.string.ticker_ask_prefix, ticker.getAsk()));
            highTextView.setText(mRes.getString(R.string.ticker_high_prefix, ticker.getHigh()));
            lowTextView.setText(mRes.getString(R.string.ticker_low_prefix, ticker.getLow()));
            timestampTextView.setText(mDateFormat.format(ticker.getTimestamp()));
        }

    }
}
