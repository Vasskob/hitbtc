package com.oliver.hitbtctrader.screens.common;


import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.arellomobile.mvp.MvpFragment;
import dagger.android.AndroidInjection;


public abstract class BaseFragment extends MvpFragment{
    private ProgressDialog mLoadingDialog;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initLoadingDialog();
    }

    @Override
    public void onAttach(Activity activity) {
        AndroidInjection.inject(this);
        super.onAttach(activity);
    }

    private void initLoadingDialog() {
        mLoadingDialog = new ProgressDialog(getActivity());
    }

    public void showLoader() {
        mLoadingDialog.show();
    }

    public void dismissLoader() {
        mLoadingDialog.dismiss();
    }
}
