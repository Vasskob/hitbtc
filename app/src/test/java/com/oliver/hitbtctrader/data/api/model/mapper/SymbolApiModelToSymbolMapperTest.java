package com.oliver.hitbtctrader.data.api.model.mapper;

import com.oliver.hitbtctrader.data.api.model.content.SymbolApiModel;
import com.oliver.hitbtctrader.domain.model.Symbol;
import com.oliver.hitbtctrader.domain.test.SymbolTestData;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static junit.framework.Assert.assertTrue;

@RunWith(JUnit4.class)
public class SymbolApiModelToSymbolMapperTest {

    private SymbolApiModelToSymbolMapper mMapper;
    private SymbolTestData mData;

    @Before
    public void setup() {
        mMapper = new SymbolApiModelToSymbolMapper();
        mData = new SymbolTestData();
    }

    @Test
    public void testMapper() throws Exception {
        int index = mData.getRandomIndex();

        SymbolApiModel source = mData.getSymbolApiModel(index);
        Symbol expected = mData.getSymbolModel(index);

        Symbol result = mMapper.apply(source);

        assertTrue("Mapper converting error", expected.equals(result));

    }

}
