package com.oliver.hitbtctrader.screens.tmp.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.PresenterType;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.oliver.hitbtctrader.R;
import com.oliver.hitbtctrader.domain.TmpDataRepository;
import com.oliver.hitbtctrader.screens.common.BaseFragment;
import com.oliver.hitbtctrader.screens.tmp.presenter.TmpPresenter;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TmpFragment extends BaseFragment implements TmpView {

    private static final String TAG = TmpFragment.class.getSimpleName();

    @BindView(R.id.tv_timestamp)
    TextView mTitleTextView;

    @Inject
    TmpDataRepository mRepository;

    @InjectPresenter(type = PresenterType.LOCAL)
    TmpPresenter mPresenter;


    @ProvidePresenter(type = PresenterType.LOCAL)
    TmpPresenter provideTmpPresenter() {
        return new TmpPresenter(mRepository);
    }

    public static TmpFragment newInstance() {
        return new TmpFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_tmp, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

    }

    @OnClick(R.id.btn_get_timestamp)
    public void onGetTimestampClick() {
        mPresenter.loadTimestamp();
    }

    @Override
    public void showTimestamp(String timeStamp) {
        mTitleTextView.setText(timeStamp);
    }

    @Override
    public void showTimestampLoadingError() {
        Toast.makeText(getActivity(), R.string.err_timestamp_loading, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoadingDialog() {
        showLoader();
    }

    @Override
    public void dismissLoadingDialog() {
        dismissLoader();
    }
}
