package com.oliver.hitbtctrader.screens.tmp.presenter;

import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.oliver.hitbtctrader.domain.TmpDataRepository;
import com.oliver.hitbtctrader.screens.tmp.view.TmpView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import io.reactivex.disposables.Disposable;

@InjectViewState
public class TmpPresenter extends MvpPresenter<TmpView> {
    public static final String TAG = TmpPresenter.class.getSimpleName();
    private SimpleDateFormat mDateFormat = new SimpleDateFormat("dd/MM/YYYY HH:mm:ss:SSS", Locale.US);
    private final TmpDataRepository mRepository;
    private Disposable mTimestampDisposable;

    public TmpPresenter(TmpDataRepository repository) {
        mRepository = repository;
    }

    @Override
    public void detachView(TmpView view) {
        super.detachView(view);

        if (mTimestampDisposable != null && !mTimestampDisposable.isDisposed())
            mTimestampDisposable.dispose();

    }


    public void loadTimestamp() {
        Log.d(TAG, "loadTimestamp() called");
        mTimestampDisposable = mRepository.getTime()
                .doOnSubscribe(disposable -> getViewState().showLoadingDialog())
                .doAfterTerminate(getViewState()::dismissLoadingDialog)
                .map(time -> new Date(time.getTimestamp()))
                .map(mDateFormat::format)
                .subscribe(getViewState()::showTimestamp,
                        throwable -> {
                            Log.d(TAG, "loadTimestamp: onError");
                            throwable.printStackTrace();
                            getViewState().showTimestampLoadingError();
                        },
                        () -> Log.d(TAG, "loadTimestamp: onCompleted"));
    }
}
