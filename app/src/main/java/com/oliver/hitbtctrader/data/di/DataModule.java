package com.oliver.hitbtctrader.data.di;


import android.arch.persistence.room.Room;
import android.content.Context;

import com.oliver.hitbtctrader.data.api.ApiFactory;
import com.oliver.hitbtctrader.data.api.PublicApi;
import com.oliver.hitbtctrader.data.local.HitBTCDatabase;
import com.oliver.hitbtctrader.data.local.model.dao.SymbolsDAO;
import com.oliver.hitbtctrader.data.repository.RepositoryProvider;
import com.oliver.hitbtctrader.domain.SymbolsRepository;
import com.oliver.hitbtctrader.domain.TickerRepository;
import com.oliver.hitbtctrader.domain.TmpDataRepository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class DataModule {

    // TODO: 09/08/17 figure out how to hide api, local provides from DataModule
    // ======== API
    @Provides
    @Singleton
    public Retrofit provideRetrofit() {
        return ApiFactory.getRetrofit();
    }

    @Provides
    @Singleton
    public PublicApi providePublicApi(Retrofit retrofit) {
        return retrofit.create(PublicApi.class);
    }

    // ======== LOCAL
    @Provides
    @Singleton
    public HitBTCDatabase getDatabase(Context context){
        return Room.databaseBuilder(context,
                HitBTCDatabase.class, "hitbtc.db")
                .build();
    }

    @Provides
    @Singleton
    public SymbolsDAO getSymbolsDAO(HitBTCDatabase db){
        return db.symbolsDao();
    }

    @Provides
    @Singleton
    public TmpDataRepository provideTmpDataRepository(PublicApi api) {
        return RepositoryProvider.provideTmpDataRepository(api);
    }


    @Provides
    @Singleton
    public SymbolsRepository provideSymbolsRepository(PublicApi api, SymbolsDAO localDAO) {
        return RepositoryProvider.provideSymbolsRepository(api, localDAO);
    }

    @Provides
    @Singleton
    public TickerRepository provideTickerRepository(PublicApi api) {
        return RepositoryProvider.provideTickerRepository(api);
    }
}
