package com.oliver.hitbtctrader.app.di;


import android.app.Fragment;

import com.oliver.hitbtctrader.screens.symbols.list.di.SymbolsListSubComponent;
import com.oliver.hitbtctrader.screens.symbols.list.view.SymbolsListFragment;
import com.oliver.hitbtctrader.screens.symbols.ticker.di.SymbolTickerSubComponent;
import com.oliver.hitbtctrader.screens.symbols.ticker.view.SymbolTickerFragment;
import com.oliver.hitbtctrader.screens.tmp.di.TmpSubComponent;
import com.oliver.hitbtctrader.screens.tmp.view.TmpFragment;

import dagger.Binds;
import dagger.Module;
import dagger.android.AndroidInjector;
import dagger.android.FragmentKey;
import dagger.multibindings.IntoMap;

@Module(subcomponents = {
        SymbolsListSubComponent.class,
        SymbolTickerSubComponent.class,
        TmpSubComponent.class,
})
abstract class FragmentModule {

    @Binds
    @IntoMap
    @FragmentKey(SymbolsListFragment.class)
    abstract AndroidInjector.Factory<? extends Fragment>
    bindSymbolListFragmentInjectionFactory(SymbolsListSubComponent.Builder builder);


    @Binds
    @IntoMap
    @FragmentKey(SymbolTickerFragment.class)
    abstract AndroidInjector.Factory<? extends Fragment>
    bindSymbolTickerFragmentInjectionFactory(SymbolTickerSubComponent.Builder builder);


    @Binds
    @IntoMap
    @FragmentKey(TmpFragment.class)
    abstract AndroidInjector.Factory<? extends Fragment>
    bindTmpFragmentInjectionFactory(TmpSubComponent.Builder builder);


}
