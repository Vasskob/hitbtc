package com.oliver.hitbtctrader.data.repository;


import com.oliver.hitbtctrader.data.api.PublicApi;
import com.oliver.hitbtctrader.data.local.model.dao.SymbolsDAO;
import com.oliver.hitbtctrader.domain.SymbolsRepository;
import com.oliver.hitbtctrader.domain.TickerRepository;
import com.oliver.hitbtctrader.domain.TmpDataRepository;

public abstract class RepositoryProvider {


    public static TmpDataRepository provideTmpDataRepository(PublicApi api) {
        return new MockTmpDataRepositoryImpl(api);
    }

    public static SymbolsRepository provideSymbolsRepository(PublicApi api, SymbolsDAO localDAO) {
        return new MockSymbolsRepositoryImpl(api, localDAO);
    }

    public static TickerRepository provideTickerRepository(PublicApi api) {
        return new MockTickerRepositoryImpl(api);
    }
}
