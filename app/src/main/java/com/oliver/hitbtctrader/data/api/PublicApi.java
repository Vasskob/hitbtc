package com.oliver.hitbtctrader.data.api;

import com.oliver.hitbtctrader.BuildConfig;
import com.oliver.hitbtctrader.data.api.model.content.TickerApiModel;
import com.oliver.hitbtctrader.data.api.model.content.TimeApiModel;
import com.oliver.hitbtctrader.data.api.model.responces.SymbolsListResponse;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;


/**
 * Created by oliver on 6/12/17.
 */

public interface PublicApi {
    String PREFIX = "public/";
    String PATH = BuildConfig.API + BuildConfig.VERSION + PREFIX;

    @GET(PATH + "time")
    Observable<TimeApiModel> getTimestamp();

    @GET(PATH + "symbols")
    Flowable<SymbolsListResponse> getSymbolsList();

    @GET(PATH + "{symbol}/ticker")
    Observable<TickerApiModel> getSymbolTicker(@Path("symbol") String symbol);
}
