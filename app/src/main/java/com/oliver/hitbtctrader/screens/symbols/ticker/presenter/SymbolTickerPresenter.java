package com.oliver.hitbtctrader.screens.symbols.ticker.presenter;


import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.oliver.hitbtctrader.domain.TickerRepository;
import com.oliver.hitbtctrader.domain.model.Symbol;
import com.oliver.hitbtctrader.screens.symbols.ticker.view.SymbolTickerView;

import io.reactivex.disposables.Disposable;

@InjectViewState
public class SymbolTickerPresenter extends MvpPresenter<SymbolTickerView> {

    private static final String TAG = SymbolTickerPresenter.class.getSimpleName();
    private final TickerRepository mRepository;

    private Disposable mSymbolTickerDisposable;

    public SymbolTickerPresenter(TickerRepository repository) {
        mRepository = repository;
    }


    @Override
    public void detachView(SymbolTickerView view) {
        super.detachView(view);


        if (mSymbolTickerDisposable != null && !mSymbolTickerDisposable.isDisposed())
            mSymbolTickerDisposable.dispose();
    }

    public void loadSymbolTicker(Symbol symbol) {
        Log.d(TAG, "loadSymbolsList: ");

        mSymbolTickerDisposable = mRepository.getTicker(symbol.getSymbol())
                .doOnSubscribe(disposable -> getViewState().showLoadingDialog())
                .doAfterTerminate(getViewState()::dismissLoadingDialog)
                .subscribe(ticker -> {
                            Log.d(TAG, "loadSymbolTicker: list: " + ticker);
                            getViewState().showTicker(ticker);
                        },
                        throwable -> {
                            throwable.printStackTrace();
                            getViewState().showLoadingError();
                        });
    }

}
