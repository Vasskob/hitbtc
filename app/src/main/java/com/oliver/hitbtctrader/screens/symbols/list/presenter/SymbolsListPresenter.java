package com.oliver.hitbtctrader.screens.symbols.list.presenter;


import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.oliver.hitbtctrader.domain.SymbolsRepository;
import com.oliver.hitbtctrader.screens.symbols.list.view.SymbolsListView;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

@InjectViewState
public class SymbolsListPresenter extends MvpPresenter<SymbolsListView> {

    private Disposable mSymbolsListDisposable;
    private SymbolsRepository mRepository;

    public SymbolsListPresenter(SymbolsRepository repository) {
        mRepository = repository;
    }


    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        // TODO: 10/08/17 not sure about this,
        getViewState().onFirstViewAttach();
    }

    @Override
    public void detachView(SymbolsListView view) {
        super.detachView(view);

        if (mSymbolsListDisposable != null && !mSymbolsListDisposable.isDisposed())
            mSymbolsListDisposable.dispose();
    }

    public void loadSymbolsList() {

        mSymbolsListDisposable = mRepository.getSymbolsList()
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(disposable -> doONSubscribe())
                .doAfterTerminate(this::doAfterTerminate)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(symbols -> Timber.d("loadSymbolsList: " + symbols))
                .subscribe(
                        symbolsList -> getViewState().showSymbolsList(symbolsList),
                        throwable -> {
                            throwable.printStackTrace();
                            getViewState().showLoadingError();
                        });
    }

    private void doAfterTerminate() {
        Timber.d("doAfterTerminate");
        getViewState().dismissLoadingDialog();
    }

    private void doONSubscribe() {
        Timber.d("doONSubscribe");
        getViewState().showLoadingDialog();
    }

}
