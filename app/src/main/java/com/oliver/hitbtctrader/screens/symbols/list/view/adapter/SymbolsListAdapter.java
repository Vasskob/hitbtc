package com.oliver.hitbtctrader.screens.symbols.list.view.adapter;


import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oliver.hitbtctrader.R;
import com.oliver.hitbtctrader.domain.model.Symbol;
import com.oliver.hitbtctrader.screens.common.OnItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SymbolsListAdapter extends RecyclerView.Adapter<SymbolsListAdapter.SymbolItemViewHolder> {

    private final OnItemClickListener<Symbol> mListener;
    private List<Symbol> mData = new ArrayList<>();

    public SymbolsListAdapter(OnItemClickListener<Symbol> listener) {
        mListener = listener;
    }

    public void setData(@NonNull List<Symbol> data) {
       mData.clear();
       mData.addAll(data);
       notifyDataSetChanged();
    }

    public void addData(@NonNull List<Symbol> data) {
        int range = mData.size();
        mData.addAll(data);
        notifyItemRangeInserted(range, data.size());
    }

    public void clearData() {
        int range = mData.size();
        mData.clear();
        notifyItemRangeRemoved(0, range);
    }

    private void holderClicked(SymbolItemViewHolder holder) {
        if (mListener != null) {
            mListener.onItemClick(holder.itemView, mData.get(holder.getAdapterPosition()));
        }
    }

    @Override
    public SymbolItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SymbolItemViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_symbol, parent, false));
    }

    @Override
    public void onBindViewHolder(SymbolItemViewHolder holder, int position) {
        Symbol symbol = mData.get(position);
        holder.bind(symbol);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    class SymbolItemViewHolder extends RecyclerView.ViewHolder {
        private final Resources mRes;
        @BindView(R.id.tv_symbol)
        TextView symbolTextView;
        @BindView(R.id.tv_step)
        TextView stepTextView;
        @BindView(R.id.tv_lot)
        TextView lotTextView;
        @BindView(R.id.tv_currency)
        TextView currencyTextView;
        @BindView(R.id.tv_commodity)
        TextView commodityTextView;
        @BindView(R.id.tv_take_liquidity_rate)
        TextView takeLiquidityRateTextView;
        @BindView(R.id.tv_provide_liquidity_rate)
        TextView provideLiquidityRateTextView;

        public SymbolItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            mRes = itemView.getResources();
        }

        public void bind(Symbol symbol) {
            symbolTextView.setText(symbol.getSymbol());
            stepTextView.setText(mRes.getString(R.string.symbol_step_prefix, symbol.getStep()));
            lotTextView.setText(mRes.getString(R.string.symbol_lot_prefix, symbol.getLot()));
            currencyTextView.setText(symbol.getCurrency());
            commodityTextView.setText(symbol.getCommodity());
            takeLiquidityRateTextView.setText(mRes.getString(R.string.symbol_take_rate_prefix, symbol.getTakeLiquidityRate()));
            provideLiquidityRateTextView.setText(mRes.getString(R.string.symbol_provide_rate_prefix, symbol.getProvideLiquidityRate()));
        }

        @OnClick(R.id.cl_symbol_item_root)
        public void onRootClick() {
            holderClicked(SymbolItemViewHolder.this);
        }
    }
}
