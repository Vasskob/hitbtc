package com.oliver.hitbtctrader.app.di;

import android.content.Context;

import com.oliver.hitbtctrader.app.HitBTCApplication;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    @Provides
    @Singleton
    public Context provideContext(HitBTCApplication application) {
        return application.getApplicationContext();
    }
}
