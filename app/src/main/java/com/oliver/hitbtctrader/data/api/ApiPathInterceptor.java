package com.oliver.hitbtctrader.data.api;


import android.util.Log;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class ApiPathInterceptor implements Interceptor{
    private static final String TAG = ApiPathInterceptor.class.getSimpleName();

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        HttpUrl url = request.url().newBuilder()
//                .addEncodedPathSegment("test/")
//                .addQueryParameter("api_key", BuildConfig.API_KEY)
                .build();
        request = request.newBuilder().url(url).build();
        Log.d(TAG, "intercept: url: " +url);
        return chain.proceed(request);
    }
}
