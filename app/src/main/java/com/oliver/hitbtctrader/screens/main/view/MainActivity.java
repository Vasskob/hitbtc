package com.oliver.hitbtctrader.screens.main.view;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.oliver.hitbtctrader.R;
import com.oliver.hitbtctrader.domain.model.Symbol;
import com.oliver.hitbtctrader.screens.symbols.list.view.SymbolsListFragment;
import com.oliver.hitbtctrader.screens.symbols.ticker.view.SymbolTickerFragment;
import com.oliver.hitbtctrader.screens.tmp.view.TmpFragment;

import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements SymbolsListFragment.OnSymbolsListInteractionListener {

    private static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.menu_timestamp:
                showTimestamp();
                return true;
            case R.id.menu_symbols_list:
                showSymbolsList();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showTimestamp() {
        showFragment(TmpFragment.newInstance(), false);
    }


    public void showSymbolsList() {
        showFragment(SymbolsListFragment.newInstance(), false);
    }

    private void showFragment(Fragment fragment, boolean addToBackStack) {
        FragmentManager fManager = getFragmentManager();
        FragmentTransaction fTransaction = fManager.beginTransaction();
        fTransaction.replace(R.id.fl_fragment_container, fragment, fragment.getClass().getSimpleName());
        if (addToBackStack) {
            fTransaction.addToBackStack(fragment.getClass().getSimpleName());
        }
        fTransaction.commit();
    }

    @Override
    public void onSymbolSelected(Symbol symbol) {
        Log.d(TAG, "onSymbolSelected: symbol: " + symbol);
        showFragment(SymbolTickerFragment.newInstance(symbol), true);
    }
}
