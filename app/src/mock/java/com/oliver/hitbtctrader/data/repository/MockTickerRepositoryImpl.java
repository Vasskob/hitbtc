package com.oliver.hitbtctrader.data.repository;


import com.oliver.hitbtctrader.data.api.PublicApi;
import com.oliver.hitbtctrader.domain.TickerRepository;
import com.oliver.hitbtctrader.domain.model.Ticker;
import com.oliver.hitbtctrader.domain.test.TickerTestData;

import io.reactivex.Observable;

public class MockTickerRepositoryImpl implements TickerRepository {


    private final TickerTestData mTickerTestData;

    public MockTickerRepositoryImpl(PublicApi api) {
        mTickerTestData = new TickerTestData();
    }

    @Override
    public Observable<Ticker> getTicker(String symbol) {
        return Observable.just(mTickerTestData.getRandomTickerModel());
    }
}
