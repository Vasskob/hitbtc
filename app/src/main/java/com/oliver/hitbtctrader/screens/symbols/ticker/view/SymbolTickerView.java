package com.oliver.hitbtctrader.screens.symbols.ticker.view;


import com.oliver.hitbtctrader.domain.model.Ticker;
import com.oliver.hitbtctrader.screens.common.LoadingView;

public interface SymbolTickerView extends LoadingView {

    void showTicker(Ticker ticker);

    void showLoadingError();
}
