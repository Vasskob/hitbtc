package com.oliver.hitbtctrader.data.local.model.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.oliver.hitbtctrader.data.local.model.content.SymbolEntity;

import java.util.List;

import io.reactivex.Single;

@Dao
public interface SymbolsDAO {

    @Query("SELECT * FROM SymbolEntity")
    Single<List<SymbolEntity>> getSymbols();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(SymbolEntity... symbols);

    @Delete
    void deleteSymbol(SymbolEntity symbol);

    @Query("DELETE FROM SymbolEntity")
    void deleteAllSymbols();
}
