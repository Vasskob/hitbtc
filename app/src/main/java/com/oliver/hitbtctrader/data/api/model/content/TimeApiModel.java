package com.oliver.hitbtctrader.data.api.model.content;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by oliver on 6/12/17.
 */

public class TimeApiModel {

    @SerializedName("timestamp")
    @Expose
    private long timestamp;

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "TimeApiModel{" +
                "timestamp=" + timestamp +
                '}';
    }
}
