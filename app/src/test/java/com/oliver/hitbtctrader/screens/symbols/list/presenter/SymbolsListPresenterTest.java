package com.oliver.hitbtctrader.screens.symbols.list.presenter;

import com.oliver.hitbtctrader.domain.SymbolsRepository;
import com.oliver.hitbtctrader.domain.model.Symbol;
import com.oliver.hitbtctrader.rules.RxImmediateSchedulerRule;
import com.oliver.hitbtctrader.screens.symbols.list.view.SymbolsListView;

import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Flowable;

import static junit.framework.Assert.assertNotNull;

@RunWith(JUnit4.class)
public class SymbolsListPresenterTest {

    @ClassRule
    public static final RxImmediateSchedulerRule schedulers = new RxImmediateSchedulerRule();

    private final List<Symbol> VALID_RESPONSE = new ArrayList<>();
    {
        VALID_RESPONSE.add(new Symbol());
        VALID_RESPONSE.add(new Symbol());
        VALID_RESPONSE.add(new Symbol());
    }

    private SymbolsListPresenter mPresenter;

    @Mock
    private SymbolsListView mView;

    @Mock
    private SymbolsRepository mRepository;

    @Before
    public void setup() {
        mView = Mockito.mock(SymbolsListView.class);

        mRepository = Mockito.mock(SymbolsRepository.class);

        mPresenter = new SymbolsListPresenter(mRepository);
        mPresenter.attachView(mView);
    }

    @After
    public void tearDown() {
        mPresenter.detachView(mView);
        mPresenter = null;
    }

    @Test
    public void presenterCreatedTest() {
        assertNotNull(mPresenter);
    }
//
//    todo test failed because in view interaction in setup method onFirstViewAttached
//    @Test
//    public void testNoActionsWithView() throws Exception {
//        mPresenter.detachView(mView);
//        Mockito.verifyNoMoreInteractions(mView);
//    }

    @Test
    public void loadSymbolsListShowDismissLoading() {
        Mockito.when(mRepository.getSymbolsList())
                .thenReturn(Flowable.empty());

        mPresenter.loadSymbolsList();
        Mockito.verify(mView).showLoadingDialog();

        Mockito.verify(mView).dismissLoadingDialog();
    }


    @Test
    public void loadSymbolsListSuccess() {
        Mockito.when(mRepository.getSymbolsList())
                .thenReturn(Flowable.just(VALID_RESPONSE));

        mPresenter.loadSymbolsList();
        Mockito.verify(mView).showSymbolsList(VALID_RESPONSE);
    }

    @Test
    public void loadSymbolsListError() {
        Mockito.when(mRepository.getSymbolsList())
                .thenReturn(Flowable.error(new RuntimeException()));

        mPresenter.loadSymbolsList();

        Mockito.verify(mView).showLoadingError();
    }
}
