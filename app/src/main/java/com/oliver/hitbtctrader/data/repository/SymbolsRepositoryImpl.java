package com.oliver.hitbtctrader.data.repository;


import com.oliver.hitbtctrader.data.api.PublicApi;
import com.oliver.hitbtctrader.data.api.model.content.SymbolApiModel;
import com.oliver.hitbtctrader.data.api.model.mapper.SymbolApiModelToSymbolMapper;
import com.oliver.hitbtctrader.data.api.model.responces.SymbolsListResponse;
import com.oliver.hitbtctrader.data.local.SymbolsCacheTransformer;
import com.oliver.hitbtctrader.data.local.model.dao.SymbolsDAO;
import com.oliver.hitbtctrader.domain.SymbolsRepository;
import com.oliver.hitbtctrader.domain.model.Symbol;

import org.reactivestreams.Publisher;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.FlowableTransformer;

public class SymbolsRepositoryImpl implements SymbolsRepository {

    private final PublicApi mApi;
    private final SymbolsDAO mDao;
    private final SymbolsCacheTransformer mSymbolsCacheTransformer;

    @Inject
    public SymbolsRepositoryImpl(PublicApi api, SymbolsDAO localDao) {
        mApi = api;
        mDao = localDao;
        // TODO: 8/10/2017 add symbol cache transformer
        mSymbolsCacheTransformer = new SymbolsCacheTransformer(localDao);
    }

    @Override
    public Flowable<List<Symbol>> getSymbolsList() {
        return mApi.getSymbolsList()
                .map(SymbolsListResponse::getSymbols)
//                .compose(new SymbolsApiToDomainTransformer())
                .compose(mSymbolsCacheTransformer)
                ;
    }

    // TODO: 8/10/2017 check when rx optimization
    static class SymbolsApiToDomainTransformer implements FlowableTransformer<List<SymbolApiModel>, List<Symbol>> {

        @Override
        public Publisher<List<Symbol>> apply(Flowable<List<SymbolApiModel>> upstream) {
            return upstream
                    .flatMap(Flowable::fromIterable)
                    .map(new SymbolApiModelToSymbolMapper())
                    .toList()
                    .toFlowable();
        }
    }
}
