package com.oliver.hitbtctrader.domain.test;


import com.oliver.hitbtctrader.data.api.model.content.TimeApiModel;
import com.oliver.hitbtctrader.domain.model.Time;

public class TimeTestData {

    public static final Long TIMESTAMP = 1393492619000L;

    public static TimeApiModel getTimeApiModel() {
        TimeApiModel result = new TimeApiModel();
        result.setTimestamp(TIMESTAMP);
        return result;
    }

    public static Time getTimeModel() {
        Time result = new Time();

        result.setTimestamp(TIMESTAMP);
        return result;
    }
}
