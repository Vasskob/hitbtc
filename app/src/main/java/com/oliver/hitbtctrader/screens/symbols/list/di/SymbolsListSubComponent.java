package com.oliver.hitbtctrader.screens.symbols.list.di;


import com.oliver.hitbtctrader.screens.symbols.list.view.SymbolsListFragment;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;

@Subcomponent
public interface SymbolsListSubComponent extends AndroidInjector<SymbolsListFragment> {
    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<SymbolsListFragment> {}
}
