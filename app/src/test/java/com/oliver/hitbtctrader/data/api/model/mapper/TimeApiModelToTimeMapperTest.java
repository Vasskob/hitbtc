package com.oliver.hitbtctrader.data.api.model.mapper;

import com.oliver.hitbtctrader.data.api.model.content.TimeApiModel;
import com.oliver.hitbtctrader.domain.model.Time;
import com.oliver.hitbtctrader.domain.test.TimeTestData;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static junit.framework.Assert.assertTrue;

@RunWith(JUnit4.class)
public class TimeApiModelToTimeMapperTest {

    private TimeApiModelToTimeMapper mMapper;

    @Before
    public void setup() {
        mMapper = new TimeApiModelToTimeMapper();
    }

    @Test
    public void testMapper() throws Exception {
        TimeApiModel sourceModel = TimeTestData.getTimeApiModel();
        Time expectedModel = TimeTestData.getTimeModel();

        Time resultModel = mMapper.apply(sourceModel);

        assertTrue("Mapper converting error", expectedModel.equals(resultModel));

    }
}
