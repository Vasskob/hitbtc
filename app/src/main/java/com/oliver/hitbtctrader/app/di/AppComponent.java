package com.oliver.hitbtctrader.app.di;

import com.oliver.hitbtctrader.app.HitBTCApplication;
import com.oliver.hitbtctrader.data.di.DataModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;

@Component(modules = {
        AppModule.class,
        DataModule.class,

        AndroidInjectionModule.class,
        FragmentModule.class
})
@Singleton
public interface AppComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder application(HitBTCApplication application);

        AppComponent build();
    }

    void inject(HitBTCApplication application);
}
