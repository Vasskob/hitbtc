package com.oliver.hitbtctrader.domain.test;


import com.oliver.hitbtctrader.data.api.model.content.SymbolApiModel;
import com.oliver.hitbtctrader.data.local.model.content.SymbolEntity;
import com.oliver.hitbtctrader.domain.model.Symbol;

import java.util.Random;

public class SymbolTestData {
    private final int SIZE = 5;

    private final String[] SYMBOL = {"BCNBTC", "BTCEUR", "BTCUSD", "DASHBTC", "ETHBTC"};
    private final String[] CURRENCY = {"BTC", "EUR", "USD", "BTC", "BTC"};
    private final String[] COMMODITY = {"BCN", "BTC", "BTC", "DASH", "ETH"};
    private final String[] STEP = {"0.1", "0.02", "0.003", "0.0004", "0.00005"};
    private final String[] LOT = {"1", "200", "3000", "40000", "50000"};
    private final String[] TAKE_LIQUIDITY_RATE = {"0.1", "0.02", "0.003", "0.0004", "0.00005"};
    private final String[] PROVIDE_LIQUIDITY_RATE = {"-0.1", "-0.02", "-0.003", "-0.0004", "-0.00005"};

    private Random r = new Random();

    public SymbolApiModel getSymbolApiModel(int index) {
        SymbolApiModel result = new SymbolApiModel();
        result.setSymbol(SYMBOL[index]);
        result.setStep(STEP[index]);
        result.setLot(LOT[index]);
        result.setCurrency(CURRENCY[index]);
        result.setCommodity(COMMODITY[index]);
        result.setTakeLiquidityRate(TAKE_LIQUIDITY_RATE[index]);
        result.setProvideLiquidityRate(PROVIDE_LIQUIDITY_RATE[index]);
        return result;
    }

    public Symbol getSymbolModel(int index) {
        Symbol result = new Symbol();
        result.setSymbol(SYMBOL[index]);
        result.setStep(STEP[index]);
        result.setLot(LOT[index]);
        result.setCurrency(CURRENCY[index]);
        result.setCommodity(COMMODITY[index]);
        result.setTakeLiquidityRate(TAKE_LIQUIDITY_RATE[index]);
        result.setProvideLiquidityRate(PROVIDE_LIQUIDITY_RATE[index]);
        return result;
    }


    public SymbolEntity getSymbolEntity(int index) {
        SymbolEntity result = new SymbolEntity();
        result.setSymbol(SYMBOL[index]);
        result.setStep(STEP[index]);
        result.setLot(LOT[index]);
        result.setCurrency(CURRENCY[index]);
        result.setCommodity(COMMODITY[index]);
        result.setTakeLiquidityRate(TAKE_LIQUIDITY_RATE[index]);
        result.setProvideLiquidityRate(PROVIDE_LIQUIDITY_RATE[index]);
        return result;
    }

    public Symbol getRandomSymbol() {
        return getSymbolModel(getRandomIndex());
    }

    public int getRandomIndex() {
        return r.nextInt(SIZE);
    }

}
