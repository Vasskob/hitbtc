package com.oliver.hitbtctrader.data.local;


import com.oliver.hitbtctrader.data.api.model.content.SymbolApiModel;
import com.oliver.hitbtctrader.data.local.model.content.SymbolEntity;
import com.oliver.hitbtctrader.data.local.model.dao.SymbolsDAO;
import com.oliver.hitbtctrader.data.local.model.mapper.SymbolApiModelToSymbolEntityMapper;
import com.oliver.hitbtctrader.data.local.model.mapper.SymbolEntityToSymbolMapper;
import com.oliver.hitbtctrader.domain.model.Symbol;

import org.reactivestreams.Publisher;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.FlowableTransformer;

public class SymbolsCacheTransformer implements FlowableTransformer<List<SymbolApiModel>, List<Symbol>> {

    private final SymbolsDAO mDAO;
    private final FromSymbolEntityTransformer mFromSymbolEntityTransformer;
    private final ToSymbolEntityTransformer mToSymbolEntityTransformer;


    public SymbolsCacheTransformer(SymbolsDAO dao) {
        mDAO = dao;
        mFromSymbolEntityTransformer = new FromSymbolEntityTransformer();
        mToSymbolEntityTransformer = new ToSymbolEntityTransformer();
    }

    // TODO: 8/10/2017 check if can be simplified
    @Override
    public Publisher<List<Symbol>> apply(Flowable<List<SymbolApiModel>> upstream) {
        return upstream
                .compose(mToSymbolEntityTransformer)
                .map(this::cache)
                .compose(mFromSymbolEntityTransformer)
                .onErrorResumeNext(this::loadFromCache);
    }

    private Flowable<List<Symbol>> loadFromCache(Throwable throwable) {
        return mDAO.getSymbols()
                .toFlowable()
                .compose(mFromSymbolEntityTransformer);
    }

    private List<SymbolEntity> cache(List<SymbolEntity> symbolEntities) {
//        mDAO.deleteAllSymbols();
        mDAO.insert(symbolEntities.toArray(new SymbolEntity[symbolEntities.size()]));
        return symbolEntities;
    }

    static class FromSymbolEntityTransformer implements FlowableTransformer<List<SymbolEntity>, List<Symbol>> {

        @Override
        public Publisher<List<Symbol>> apply(Flowable<List<SymbolEntity>> upstream) {
            return upstream
                    .flatMap(Flowable::fromIterable)
                    .map( new SymbolEntityToSymbolMapper())
                    .toList()
                    .toFlowable();
        }
    }


    static class ToSymbolEntityTransformer implements FlowableTransformer<List<SymbolApiModel>, List<SymbolEntity>> {

        @Override
        public Publisher<List<SymbolEntity>> apply(Flowable<List<SymbolApiModel>> upstream) {
            return upstream
                    .flatMap(Flowable::fromIterable)
                    .map(new SymbolApiModelToSymbolEntityMapper())
                    .toList()
                    .toFlowable();
        }
    }
}
