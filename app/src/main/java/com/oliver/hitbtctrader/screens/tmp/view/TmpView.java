package com.oliver.hitbtctrader.screens.tmp.view;

import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.oliver.hitbtctrader.screens.common.LoadingView;

@StateStrategyType(AddToEndSingleStrategy.class)
public interface TmpView extends LoadingView {

    void showTimestamp(String timeStamp);

    @StateStrategyType(OneExecutionStateStrategy.class)
    void showTimestampLoadingError();
}
