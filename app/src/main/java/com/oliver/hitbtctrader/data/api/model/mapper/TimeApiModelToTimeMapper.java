package com.oliver.hitbtctrader.data.api.model.mapper;


import com.oliver.hitbtctrader.data.api.model.content.TimeApiModel;
import com.oliver.hitbtctrader.domain.model.Time;

import io.reactivex.functions.Function;

public class TimeApiModelToTimeMapper implements Function<TimeApiModel, Time> {

    @Override
    public Time apply(TimeApiModel o) throws Exception {
        Time time = new Time();
        time.setTimestamp(o.getTimestamp());
        return time;
    }
}
