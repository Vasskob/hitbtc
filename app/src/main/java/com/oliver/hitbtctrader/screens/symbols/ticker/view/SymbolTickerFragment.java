package com.oliver.hitbtctrader.screens.symbols.ticker.view;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.PresenterType;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.oliver.hitbtctrader.R;
import com.oliver.hitbtctrader.domain.TickerRepository;
import com.oliver.hitbtctrader.domain.model.Symbol;
import com.oliver.hitbtctrader.domain.model.Ticker;
import com.oliver.hitbtctrader.screens.common.BaseFragment;
import com.oliver.hitbtctrader.screens.symbols.ticker.presenter.SymbolTickerPresenter;
import com.oliver.hitbtctrader.screens.symbols.ticker.view.adapter.TickerListAdapter;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SymbolTickerFragment extends BaseFragment implements SymbolTickerView {

    private static final String TAG = SymbolTickerFragment.class.getSimpleName();
    private static final String ARG_SYMBOL = "ARG_SYMBOL";

    @BindView(R.id.tv_symbol)
    TextView symbolTextView;
    @BindView(R.id.tv_step)
    TextView stepTextView;
    @BindView(R.id.tv_lot)
    TextView lotTextView;
    @BindView(R.id.tv_currency)
    TextView currencyTextView;
    @BindView(R.id.tv_commodity)
    TextView commodityTextView;
    @BindView(R.id.tv_take_liquidity_rate)
    TextView takeLiquidityRateTextView;
    @BindView(R.id.tv_provide_liquidity_rate)
    TextView provideLiquidityRateTextView;
    @BindView(R.id.rv_ticker_list)
    RecyclerView mTickerListRecyclerView;

    @Inject
    TickerRepository mRepository;

    @InjectPresenter(type = PresenterType.LOCAL)
    SymbolTickerPresenter mPresenter;

    private Symbol mCurrentSymbol;
    private TickerListAdapter mAdapter;

    @ProvidePresenter(type = PresenterType.LOCAL)
    SymbolTickerPresenter provideTmpPresenter() {
        return new SymbolTickerPresenter(mRepository);
    }

    public static SymbolTickerFragment newInstance(Symbol symbol) {
        Bundle args = new Bundle();
        args.putParcelable(ARG_SYMBOL, symbol);

        SymbolTickerFragment fragment = new SymbolTickerFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_symbol_ticker, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        mCurrentSymbol = getArguments().getParcelable(ARG_SYMBOL);
        showSymbol(mCurrentSymbol);
        initList();
    }

    public void showSymbol(Symbol symbol) {
        symbolTextView.setText(symbol.getSymbol());
        stepTextView.setText(symbol.getStep());
        lotTextView.setText(symbol.getLot());
        currencyTextView.setText(symbol.getCurrency());
        commodityTextView.setText(symbol.getCommodity());
        takeLiquidityRateTextView.setText(symbol.getTakeLiquidityRate());
        provideLiquidityRateTextView.setText(symbol.getProvideLiquidityRate());
    }


    private void initList() {
        mAdapter = new TickerListAdapter();
        mTickerListRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        mTickerListRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        mTickerListRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mTickerListRecyclerView.setAdapter(mAdapter);
    }

    @OnClick (R.id.btn_load_symbol_ticker)
    public void onLoadTickerClick() {
        mPresenter.loadSymbolTicker(mCurrentSymbol);
    }

    @Override
    public void showTicker(Ticker ticker) {
        Log.d(TAG, "showTicker() called with: ticker = [" + ticker + "]");
        mAdapter.addOnTop(ticker);
    }

    @Override
    public void showLoadingError() {
        Toast.makeText(getActivity(), R.string.err_ticker_loading, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoadingDialog() {
        showLoader();
    }

    @Override
    public void dismissLoadingDialog() {
        dismissLoader();
    }
}
