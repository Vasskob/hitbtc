package com.oliver.hitbtctrader.data.api.model.mapper;


import com.oliver.hitbtctrader.data.api.model.content.TickerApiModel;
import com.oliver.hitbtctrader.domain.model.Ticker;

import io.reactivex.functions.Function;

public class TickerApiModelToTickerMapper implements Function<TickerApiModel, Ticker> {

    @Override
    public Ticker apply(TickerApiModel o) throws Exception {
        Ticker ticker = new Ticker();
        ticker.setLast(o.getLast());
        ticker.setBid(o.getBid());
        ticker.setAsk(o.getAsk());
        ticker.setHigh(o.getHigh());
        ticker.setLow(o.getLow());
        ticker.setVolume(o.getVolume());
        ticker.setOpen(o.getOpen());
        ticker.setVolumeQuote(o.getVolumeQuote());
        ticker.setTimestamp(o.getTimestamp());
        return ticker;
    }
}
