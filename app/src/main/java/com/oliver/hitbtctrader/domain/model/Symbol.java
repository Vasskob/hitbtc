package com.oliver.hitbtctrader.domain.model;


import android.os.Parcel;
import android.os.Parcelable;

// TODO: 8/10/2017 can be parcelable here or not?
public class Symbol implements Parcelable {

    private String symbol;
    private String step;
    private String lot;
    private String currency;
    private String commodity;
    private String takeLiquidityRate;
    private String provideLiquidityRate;

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getStep() {
        return step;
    }

    public void setStep(String step) {
        this.step = step;
    }

    public String getLot() {
        return lot;
    }

    public void setLot(String lot) {
        this.lot = lot;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCommodity() {
        return commodity;
    }

    public void setCommodity(String commodity) {
        this.commodity = commodity;
    }

    public String getTakeLiquidityRate() {
        return takeLiquidityRate;
    }

    public void setTakeLiquidityRate(String takeLiquidityRate) {
        this.takeLiquidityRate = takeLiquidityRate;
    }

    public String getProvideLiquidityRate() {
        return provideLiquidityRate;
    }

    public void setProvideLiquidityRate(String provideLiquidityRate) {
        this.provideLiquidityRate = provideLiquidityRate;
    }


    @Override
    public String toString() {
        return "Symbol::" +
                "symbol='" + symbol + '\'' +
                ", step='" + step + '\'' +
                ", lot='" + lot + '\'' +
                ", currency='" + currency + '\'' +
                ", commodity='" + commodity + '\'' +
                ", takeLiquidityRate='" + takeLiquidityRate + '\'' +
                ", provideLiquidityRate='" + provideLiquidityRate + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.symbol);
        dest.writeString(this.step);
        dest.writeString(this.lot);
        dest.writeString(this.currency);
        dest.writeString(this.commodity);
        dest.writeString(this.takeLiquidityRate);
        dest.writeString(this.provideLiquidityRate);
    }

    public Symbol() {
    }

    protected Symbol(Parcel in) {
        this.symbol = in.readString();
        this.step = in.readString();
        this.lot = in.readString();
        this.currency = in.readString();
        this.commodity = in.readString();
        this.takeLiquidityRate = in.readString();
        this.provideLiquidityRate = in.readString();
    }

    public static final Parcelable.Creator<Symbol> CREATOR = new Parcelable.Creator<Symbol>() {
        @Override
        public Symbol createFromParcel(Parcel source) {
            return new Symbol(source);
        }

        @Override
        public Symbol[] newArray(int size) {
            return new Symbol[size];
        }
    };

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Symbol)) return false;

        Symbol symbol1 = (Symbol) o;

        if (symbol != null ? !symbol.equals(symbol1.symbol) : symbol1.symbol != null) return false;
        if (step != null ? !step.equals(symbol1.step) : symbol1.step != null) return false;
        if (lot != null ? !lot.equals(symbol1.lot) : symbol1.lot != null) return false;
        if (currency != null ? !currency.equals(symbol1.currency) : symbol1.currency != null)
            return false;
        if (commodity != null ? !commodity.equals(symbol1.commodity) : symbol1.commodity != null)
            return false;
        if (takeLiquidityRate != null ? !takeLiquidityRate.equals(symbol1.takeLiquidityRate) : symbol1.takeLiquidityRate != null)
            return false;
        return provideLiquidityRate != null ? provideLiquidityRate.equals(symbol1.provideLiquidityRate) : symbol1.provideLiquidityRate == null;
    }

    @Override
    public int hashCode() {
        int result = symbol != null ? symbol.hashCode() : 0;
        result = 31 * result + (step != null ? step.hashCode() : 0);
        result = 31 * result + (lot != null ? lot.hashCode() : 0);
        result = 31 * result + (currency != null ? currency.hashCode() : 0);
        result = 31 * result + (commodity != null ? commodity.hashCode() : 0);
        result = 31 * result + (takeLiquidityRate != null ? takeLiquidityRate.hashCode() : 0);
        result = 31 * result + (provideLiquidityRate != null ? provideLiquidityRate.hashCode() : 0);
        return result;
    }
}
