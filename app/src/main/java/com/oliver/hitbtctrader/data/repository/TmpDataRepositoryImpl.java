package com.oliver.hitbtctrader.data.repository;


import com.oliver.hitbtctrader.data.api.PublicApi;
import com.oliver.hitbtctrader.data.api.model.mapper.TimeApiModelToTimeMapper;
import com.oliver.hitbtctrader.domain.TmpDataRepository;
import com.oliver.hitbtctrader.domain.model.Time;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class TmpDataRepositoryImpl implements TmpDataRepository {

    private final PublicApi mApi;

    @Inject
    public TmpDataRepositoryImpl(PublicApi api) {
        mApi = api;
    }

    @Override
    public Observable<Time> getTime() {
        return mApi.getTimestamp()
                .subscribeOn(Schedulers.io())
                .map(new TimeApiModelToTimeMapper())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
