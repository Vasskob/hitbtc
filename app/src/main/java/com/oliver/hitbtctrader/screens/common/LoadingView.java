package com.oliver.hitbtctrader.screens.common;


import com.arellomobile.mvp.MvpView;

public interface LoadingView extends MvpView{

    void showLoadingDialog();

    void dismissLoadingDialog();
}
