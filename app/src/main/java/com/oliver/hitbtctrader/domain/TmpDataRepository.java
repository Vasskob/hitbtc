package com.oliver.hitbtctrader.domain;


import com.oliver.hitbtctrader.domain.model.Time;

import io.reactivex.Observable;

public interface TmpDataRepository {
    Observable<Time> getTime();
}
