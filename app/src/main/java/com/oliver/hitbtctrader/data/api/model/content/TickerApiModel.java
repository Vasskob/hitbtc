package com.oliver.hitbtctrader.data.api.model.content;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TickerApiModel {
    /**
     * last : 550.73
     * bid : 549.56
     * ask : 554.12
     * high : 600.1
     * low : 400.7
     * volume : 567.9
     * open : 449.73
     * volume_quote : 289002.81
     * timestamp : 1393492619000
     */

    @SerializedName("last")
    @Expose
    private String last;
    @SerializedName("bid")
    @Expose
    private String bid;
    @SerializedName("ask")
    @Expose
    private String ask;
    @SerializedName("high")
    @Expose
    private String high;
    @SerializedName("low")
    @Expose
    private String low;
    @SerializedName("volume")
    @Expose
    private String volume;
    @SerializedName("open")
    @Expose
    private String open;
    @SerializedName("volume_quote")
    @Expose
    private String volumeQuote;
    @SerializedName("timestamp")
    @Expose
    private long timestamp;

    public String getLast() {
        return last;
    }

    public void setLast(String last) {
        this.last = last;
    }

    public String getBid() {
        return bid;
    }

    public void setBid(String bid) {
        this.bid = bid;
    }

    public String getAsk() {
        return ask;
    }

    public void setAsk(String ask) {
        this.ask = ask;
    }

    public String getHigh() {
        return high;
    }

    public void setHigh(String high) {
        this.high = high;
    }

    public String getLow() {
        return low;
    }

    public void setLow(String low) {
        this.low = low;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public String getOpen() {
        return open;
    }

    public void setOpen(String open) {
        this.open = open;
    }

    public String getVolumeQuote() {
        return volumeQuote;
    }

    public void setVolumeQuote(String volumeQuote) {
        this.volumeQuote = volumeQuote;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

}