package com.oliver.hitbtctrader.data.local.model.content;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;


@Entity
public class SymbolEntity {
    @PrimaryKey
    private String symbol;
    private String step;
    private String lot;
    private String currency;
    private String commodity;
    private String takeLiquidityRate;
    private String provideLiquidityRate;

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getStep() {
        return step;
    }

    public void setStep(String step) {
        this.step = step;
    }

    public String getLot() {
        return lot;
    }

    public void setLot(String lot) {
        this.lot = lot;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCommodity() {
        return commodity;
    }

    public void setCommodity(String commodity) {
        this.commodity = commodity;
    }

    public String getTakeLiquidityRate() {
        return takeLiquidityRate;
    }

    public void setTakeLiquidityRate(String takeLiquidityRate) {
        this.takeLiquidityRate = takeLiquidityRate;
    }

    public String getProvideLiquidityRate() {
        return provideLiquidityRate;
    }

    public void setProvideLiquidityRate(String provideLiquidityRate) {
        this.provideLiquidityRate = provideLiquidityRate;
    }


    @Override
    public String toString() {
        return "SymbolEntity::" +
                "symbol='" + symbol + '\'' +
                ", step='" + step + '\'' +
                ", lot='" + lot + '\'' +
                ", currency='" + currency + '\'' +
                ", commodity='" + commodity + '\'' +
                ", takeLiquidityRate='" + takeLiquidityRate + '\'' +
                ", provideLiquidityRate='" + provideLiquidityRate + '\'' +
                '}';
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SymbolEntity)) return false;

        SymbolEntity symbol1 = (SymbolEntity) o;

        if (symbol != null ? !symbol.equals(symbol1.symbol) : symbol1.symbol != null) return false;
        if (step != null ? !step.equals(symbol1.step) : symbol1.step != null) return false;
        if (lot != null ? !lot.equals(symbol1.lot) : symbol1.lot != null) return false;
        if (currency != null ? !currency.equals(symbol1.currency) : symbol1.currency != null)
            return false;
        if (commodity != null ? !commodity.equals(symbol1.commodity) : symbol1.commodity != null)
            return false;
        if (takeLiquidityRate != null ? !takeLiquidityRate.equals(symbol1.takeLiquidityRate) : symbol1.takeLiquidityRate != null)
            return false;
        return provideLiquidityRate != null ? provideLiquidityRate.equals(symbol1.provideLiquidityRate) : symbol1.provideLiquidityRate == null;
    }

    @Override
    public int hashCode() {
        int result = symbol != null ? symbol.hashCode() : 0;
        result = 31 * result + (step != null ? step.hashCode() : 0);
        result = 31 * result + (lot != null ? lot.hashCode() : 0);
        result = 31 * result + (currency != null ? currency.hashCode() : 0);
        result = 31 * result + (commodity != null ? commodity.hashCode() : 0);
        result = 31 * result + (takeLiquidityRate != null ? takeLiquidityRate.hashCode() : 0);
        result = 31 * result + (provideLiquidityRate != null ? provideLiquidityRate.hashCode() : 0);
        return result;
    }
}

