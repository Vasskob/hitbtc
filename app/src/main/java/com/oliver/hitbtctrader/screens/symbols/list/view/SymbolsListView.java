package com.oliver.hitbtctrader.screens.symbols.list.view;


import com.oliver.hitbtctrader.domain.model.Symbol;
import com.oliver.hitbtctrader.screens.common.LoadingView;

import java.util.List;

public interface SymbolsListView extends LoadingView {

    void onFirstViewAttach();

    void showSymbolsList(List<Symbol> symbolList);

    void showLoadingError();
}
